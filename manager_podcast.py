#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8
# -*- coding: utf-8 -*-

import sqlite3
import re
import os
import shutil
import requests
import api_listennotes
from datetime import date, datetime
import sys
from PIL import Image
import xmltodict
import subprocess
from dateutil.parser import parse

DATABASE = "db/podcast.db"
NAME_FORMAT = '{podcast_title}_{podcast_id}_episode_{episode_position}.mp3'
EPISODE_NAME = '#{episode_position}_{episode_title}'
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
}


def createDatabase():
    if not os.path.exists("db"):
        os.mkdir("db")
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS Podcast (id INTEGER PRIMARY KEY,id_listennotes TEXT,title TEXT, publisher TEXT, description TEXT, image TEXT, last_check INTEGER DEFAULT 0)''')
    cursor.execute('''CREATE TABLE IF NOT EXISTS Episode (id INTEGER PRIMARY KEY,id_podcast TEXT,id_listennotes TEXT,position INTEGER, title TEXT,description TEXT,url TEXT,pub_date_ms TEXT ,duration INTEGER, downloaded INTEGER DEFAULT 0,readed INTEGER DEFAULT 0)''')
    dataBase.commit()

    if(not checkColumnExist("Podcast", "rss_url")):
        addColumRssUrl()
    if(not checkColumnExist("Episode", "sync_watch")):
        addColumSyncWatch()
    if(not checkColumnExist("Episode", "rss_guid")):
        addColumRssGuid()
    dataBase.close()


def checkColumnExist(table, column):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute('PRAGMA table_info('+table+')')
    isExist = False
    for columnInfo in cursor.fetchall():
        if(columnInfo[1] == column):
            isExist = True
    return isExist


def addColumRssUrl():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute('''ALTER TABLE Podcast ADD rss_url TEXT ''')
    dataBase.commit()
    cursor.execute(
        "SELECT Podcast.id, Podcast.id_listennotes FROM Podcast")
    for podcast in cursor.fetchall():
        cursor.execute(
            "SELECT Episode.id FROM Episode WHERE Episode.id_podcast=?", (podcast[1],))
        for episode in cursor.fetchall():
            cursor.execute(
                "UPDATE Episode SET id_podcast=? WHERE id=?", (podcast[0], episode[0]))
            dataBase.commit()


def addColumSyncWatch():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute('''ALTER TABLE Episode ADD sync_watch INTEGER DEFAULT 0 ''')
    dataBase.commit()


def addColumRssGuid():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute('''ALTER TABLE Episode ADD rss_guid TEXT''')
    dataBase.commit()


def listPodcastWatch(display_readed):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    select_readed = ""
    if(display_readed == "false"):
        select_readed = " readed = 0 AND"

    cursor.execute("SELECT Podcast.id, Podcast.title,Podcast.image, COUNT(*) FROM Podcast LEFT JOIN Episode ON Episode.id_podcast = Podcast.id WHERE" +
                   select_readed+" downloaded = 1 GROUP BY Podcast.id ORDER BY Podcast.title")
    podcasts = []
    for podcast in cursor.fetchall():
        podcasts.append({
            'id': podcast[0],
            'name': podcast[1],
            'image': podcast[2],
            "nb_episodes": podcast[3]
        })
    dataBase.close()
    return podcasts


def listEpisodeWatch(id, order, display_readed):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    select_readed = ""
    if(display_readed == "false"):
        select_readed = " readed = 0 AND"

    cursor.execute("SELECT Podcast.id,Podcast.title,Episode.id,Episode.title,Episode.position,Episode.duration from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE" +
                   select_readed+" downloaded=1 And Podcast.id=? ORDER BY Episode.position "+order, (id,))
    episodes = []
    for episode in cursor.fetchall():
        episodes.append({
            'id': episode[2],
            'name': EPISODE_NAME.format(episode_position=str(episode[4]), episode_title=episode[3]),
            'position': episode[4],
            'duration': episode[5]
        })
    dataBase.close()
    return episodes


def listEpisodeSyncWatch():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Podcast.id,Podcast.title,Podcast.image,Episode.id,Episode.title,Episode.position,Episode.duration from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE downloaded=1 AND sync_watch=1 ORDER BY Podcast.title, Episode.position")
    podcasts = {}
    for episode in cursor.fetchall():
        if(episode[0] not in podcasts):
            podcasts[episode[0]] = {
                'id': episode[0],
                'name': episode[1],
                'image': episode[2],
                'episodes': []
            }
        episodes = podcasts[episode[0]]['episodes']
        episodes.append({
            'url': "/sync_watch/episode/"+str(episode[2]),
            'id': episode[3],
            'name': EPISODE_NAME.format(episode_position=str(episode[5]), episode_title=episode[4]),
            'position': episode[5],
            'duration': episode[6]
        })
    dataBase.close()
    return list(podcasts.values())


def getFilename(id):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Podcast.id,Podcast.title,Episode.position from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE Episode.id=? ORDER BY Episode.position", (id,))
    episode = cursor.fetchone()
    if(episode is not None):
        return NAME_FORMAT.format(podcast_title=re.sub(
            '[^A-Za-z0-9]+', '', episode[1]), podcast_id=str(episode[0]), episode_position=str(episode[2]))
    else:
        return None


def checkFileEpisodes():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT Podcast.id,Podcast.title,Episode.id, Episode.position from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id")
    for episode in cursor.fetchall():
        path = 'media/'+NAME_FORMAT.format(podcast_title=re.sub(
            '[^A-Za-z0-9]+', '', episode[1]), podcast_id=str(episode[0]), episode_position=str(episode[3]))
        if os.path.exists(path):
            cursor.execute(
                "UPDATE Episode SET downloaded = 1 WHERE id = ?", (episode[2],))
        else:
            cursor.execute(
                "UPDATE Episode SET downloaded = 0 WHERE id = ?", (episode[2],))
        dataBase.commit()


def readedEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("UPDATE Episode SET readed = 1 WHERE id = ?", (id,))
        dataBase.commit()
    dataBase.close()
    return {}


def notReadedEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("UPDATE Episode SET readed = 0 WHERE id = ?", (id,))
        dataBase.commit()
    return {}


def syncWatchEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute(
            "UPDATE Episode SET sync_watch = 1 WHERE id = ? AND downloaded=1", (id,))
        dataBase.commit()
    dataBase.close()
    return {}


def notSyncWatchEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("UPDATE Episode SET sync_watch = 0 WHERE id = ?", (id,))
        dataBase.commit()
    return {}


def resetSyncWatch():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("UPDATE Episode SET sync_watch = 0 WHERE readed = 1")
    dataBase.commit()
    return {}


def addPodcastListennotes(id_listennotes):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT * FROM Podcast WHERE id_listennotes = ? ", (id_listennotes,))
    if len(cursor.fetchall()) == 0:
        cursor.execute(
            "INSERT INTO Podcast (id_listennotes) VALUES (?)", (id_listennotes,))
        dataBase.commit()
    dataBase.close()
    return {}


def addPodcastRss(rss_url):
    response = requests.get(rss_url, headers=HEADERS)
    if(response.status_code == requests.codes['ok']):
        dataBase = sqlite3.connect(DATABASE)
        cursor = dataBase.cursor()
        cursor.execute("SELECT * FROM Podcast WHERE rss_url = ? ", (rss_url,))
        if len(cursor.fetchall()) == 0:
            cursor.execute(
                "INSERT INTO Podcast (rss_url) VALUES (?)", (rss_url,))
            dataBase.commit()
        dataBase.close()
        return 200
    else:
        return 404


def removePodcast(id):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Podcast.id,Podcast.title ,Episode.id,Episode.position from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE Podcast.id=?", (id,))
    for episode in cursor.fetchall():
        path = 'media/'+NAME_FORMAT.format(podcast_title=re.sub(
            '[^A-Za-z0-9]+', '', episode[1]), podcast_id=str(episode[0]), episode_position=str(episode[3]))
        if os.path.exists(path):
            print("Remove episode "+path)
            os.remove(path)
        path = 'media/'+re.sub('[^A-Za-z0-9]+', '',
                               episode[1])+'_episode_'+str(episode[3])+'.mp3'
        if os.path.exists(path):
            print("Remove episode "+path)
            os.remove(path)
    cursor.execute("SELECT  id FROM Podcast WHERE id = ? ", (id,))
    rows = cursor.fetchall()
    if len(rows) == 1:
        cursor.execute("DELETE FROM Podcast WHERE id=? ", (rows[0][0],))
        cursor.execute(
            "DELETE FROM Episode WHERE id_podcast=? ", (rows[0][0],))
        dataBase.commit()
    dataBase.close()
    return {}


def listPodcast(delta=-1, no_episodes=False):
    if (delta>=0):
        checkNewEpisodes(delta)

    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()

    cursor.execute("SELECT Podcast.id,Podcast.title, Podcast.description, Podcast.publisher, Podcast.image from Podcast LEFT JOIN Episode on Episode.id_podcast=Podcast.id GROUP BY Podcast.id ORDER BY Podcast.title")
    podcasts = []
    for podcast in cursor.fetchall():
        episodes = []
        if not no_episodes:
            cursor.execute(
                "SELECT Episode.id,Episode.title,Episode.description,Episode.position,Episode.pub_date_ms, Episode.duration, Episode.url, Episode.downloaded,Episode.readed, Episode.sync_watch from Episode WHERE Episode.id_podcast=? ORDER BY Episode.position", (podcast[0],))
            for episode in cursor.fetchall():
                episodes.append({
                    'id': episode[0],
                    'title': episode[1],
                    'description': episode[2],
                    'position': episode[3],
                    'pub_date_ms': episode[4],
                    'duration': episode[5],
                    'url_external': episode[6],
                    'downloaded': episode[7],
                    'readed': episode[8],
                    'sync_watch': episode[9],
                })
        podcasts.append({
            'id': podcast[0],
            'title': podcast[1],
            'description': podcast[2],
            'publisher': podcast[3],
            'image': podcast[4],
            'episodes': episodes
        })
    dataBase.close()
    return podcasts

def podcast(podcast_id):
    episodes = []
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute(
                "SELECT Episode.id,Episode.title,Episode.description,Episode.position,Episode.pub_date_ms, Episode.duration, Episode.url, Episode.downloaded,Episode.readed, Episode.sync_watch from Episode WHERE Episode.id_podcast=? ORDER BY Episode.position DESC", (podcast_id,))
    for episode in cursor.fetchall():
        episodes.append({
            'id': episode[0],
            'title': episode[1],
            'description': episode[2],
            'position': episode[3],
            'pub_date_ms': episode[4],
            'duration': episode[5],
            'url_external': episode[6],
            'downloaded': episode[7],
            'readed': episode[8],
            'sync_watch': episode[9],
        })
    cursor.execute("SELECT Podcast.id,Podcast.title, Podcast.description, Podcast.publisher, Podcast.image from Podcast WHERE Podcast.id=?", (podcast_id,))
    podcast = cursor.fetchone() 
    dataBase.close()
    return {
            'id': podcast[0],
            'title': podcast[1],
            'description': podcast[2],
            'publisher': podcast[3],
            'image': podcast[4],
            'episodes': episodes
        }

def checkNewEpisodes(delta):
    print("Check new episode")
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT Podcast.id,Podcast.id_listennotes,Podcast.rss_url, Podcast.last_check from Podcast")
    for podcast in cursor.fetchall():
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        someday = podcast[3]
        diff = int(timestamp) - int(someday)
        if(diff >= delta):
            if(podcast[1] is not None and api_listennotes.API_KEY not in (None, '')):
                print("request listennotes podcast "+podcast[1])
                pagination(dataBase, podcast[0], podcast[1], lastPubDateEpisode(
                    dataBase, podcast[0]))
            if(podcast[2]):
                print("request rss podcast "+podcast[2])
                parsingRss(dataBase, podcast[0], podcast[2])
    dataBase.close()
    print("End check new episode")


def pagination(dataBase, id_podcast, id_listennotes, next_episode_pub_date):
    podcast = api_listennotes.getPodcast(id_listennotes, next_episode_pub_date)

    cursor = dataBase.cursor()
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    cursor.execute("UPDATE Podcast SET title=?, description=?, publisher=?,image=? , last_check=? WHERE id=?",
                   (podcast['title'], podcast["description"], podcast["publisher"], podcast["image"], int(timestamp), id_podcast))
    dataBase.commit()

    for episode in podcast['episodes']:
        if not existEpisodeListennotes(dataBase, id_podcast, episode['id'],episode['audio']):
            position = getPositionEpisode(dataBase, id_podcast)
            cursor.execute("INSERT INTO Episode (id_podcast,id_listennotes,position,title,description,url,pub_date_ms,duration,downloaded,readed) VALUES (?,?,?,?,?,?,?,?,?,?)", (
                id_podcast, episode['id'], position, episode['title'], episode['description'], episode['audio'], episode['pub_date_ms'], episode['audio_length_sec'], 0, 0))
        else:
            cursor.execute("UPDATE Episode SET title=?,description=?,url=?,pub_date_ms=?,duration= ? WHERE id_podcast = ? AND (id_listennotes=? OR url=?) ", (
                episode['title'], episode['description'], episode['audio'], episode['pub_date_ms'], episode['audio_length_sec'], id_podcast, episode['id'],episode['audio']))
        dataBase.commit()
    if podcast['next_episode_pub_date'] is not None and podcast['next_episode_pub_date'] != podcast['latest_pub_date_ms']:
        pagination(dataBase, id_podcast, id_listennotes,
                   podcast['next_episode_pub_date'])


def existEpisodeListennotes(dataBase, id, id_listennotes,url):
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT * FROM Episode WHERE id_podcast = ? AND (id_listennotes=? OR url=?) ", (id, id_listennotes,url))
    return len(cursor.fetchall()) != 0


def parsingRss(dataBase, id_podcast, rss_url):
    response = requests.get(rss_url, headers=HEADERS)
    if(response.status_code == requests.codes['ok']):
        podcast = xmltodict.parse(response.content)['rss']['channel']
        cursor = dataBase.cursor()
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        description = None
        if('itunes:summary' in podcast.keys()):
            description = podcast['itunes:summary']
        elif('description' in podcast.keys()):
            description = podcast['description']

        image = None
        if('itunes:image' in podcast.keys()):
            image = podcast["itunes:image"]['@href']
        elif('image' in podcast.keys()):
            image_xml = podcast['image']
            if("url" in image_xml.keys()):
                image =  image_xml["url"]   
        author = None    
        if('itunes:author' in podcast.keys()):    
            author = podcast["itunes:author"]
        if(type(author) is list):
            author = author[0]
        cursor.execute("UPDATE Podcast SET title=?, description=?, publisher=?,image=? , last_check=? WHERE id=?",
                       (podcast['title'], description, author, image, int(timestamp), id_podcast))

        episodes = podcast['item']
        list_rss_guid = None
        if(isinstance(episodes, dict)):
            list_rss_guid = "\"" + \
                addEpisode(dataBase, id_podcast, episodes, 1)+"\""
        else:
            position = len(episodes)
            for episode in episodes:
                rss_guid = addEpisode(dataBase, id_podcast, episode, position)
                if(rss_guid is not None):
                    if(position == len(episodes)):
                        list_rss_guid = "\""+rss_guid+"\""
                    else:
                        list_rss_guid = list_rss_guid+",\""+rss_guid+"\""
                position -= 1
        cursor.execute(
            "DELETE FROM Episode WHERE id NOT IN ( SELECT id FROM Episode GROUP BY id_listennotes, rss_guid, id_podcast )")
        if(list_rss_guid is not None):
            cursor.execute("DELETE FROM Episode WHERE (rss_guid NOT IN (" +
                           list_rss_guid+") OR rss_guid IS NULL ) AND id_podcast=?", (id_podcast,))
        dataBase.commit()


def addEpisode(dataBase, id_podcast, episode, position):
    cursor = dataBase.cursor()
    description = None
    if('itunes:summary' in episode.keys()):
        description = episode['itunes:summary']
    elif('description' in episode.keys()):
        description = episode['description']

    pub_date_ms = 0
    if('pubDate' in episode.keys()):
        pub_date_ms = datetime.timestamp(
            parse(episode["pubDate"], ignoretz=True))*1000
    duration = 0
    if('itunes:duration' in episode.keys()):
        arrayDuration = episode["itunes:duration"].split(":")
        if(len(arrayDuration) == 3):
            duration = int(
                arrayDuration[0])*3600+int(arrayDuration[1])*60+int(arrayDuration[2])
        elif(len(arrayDuration) == 2):
            duration = int(
                arrayDuration[0])*60+int(arrayDuration[1])
        else:
            duration = int(arrayDuration[0])
    rss_guid = None
    if('enclosure' in episode.keys()):
        if(isinstance(episode['guid'], dict)):
            rss_guid = episode['guid']['#text']
        else:
            rss_guid = episode['guid']
        if existEpisodeRssGuid(dataBase, id_podcast, rss_guid):
            cursor.execute("UPDATE Episode SET title=?,position=?,description=?,url=?,pub_date_ms=?,duration=? WHERE id_podcast = ? AND rss_guid=?",
                           (episode['title'], position, description, episode['enclosure']['@url'], int(pub_date_ms), duration, id_podcast, rss_guid))
        else:
            if existEpisodeRssTitle(dataBase, id_podcast, episode['title']):
                cursor.execute("UPDATE Episode SET position=?,description=?,url=?,pub_date_ms=?,duration=?,rss_guid=? WHERE id_podcast = ? AND title=?",
                               (position, description, episode['enclosure']['@url'], int(pub_date_ms), duration, rss_guid, id_podcast, episode['title']))
            else:
                cursor.execute("INSERT INTO Episode (id_podcast,position,title,description,url,pub_date_ms,duration,rss_guid,downloaded,readed) VALUES (?,?,?,?,?,?,?,?,?,?)",
                               (id_podcast, position, episode['title'], description, episode['enclosure']['@url'], int(pub_date_ms), duration, rss_guid, 0, 0))

        dataBase.commit()
    return rss_guid


def existEpisodeRssGuid(dataBase, id, rss_guid):
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT * FROM Episode WHERE id_podcast = ? AND rss_guid=? ", (id, rss_guid))
    return len(cursor.fetchall()) != 0


def existEpisodeRssTitle(dataBase, id, title):
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT * FROM Episode WHERE id_podcast = ? AND title=? ", (id, title))
    return len(cursor.fetchall()) != 0


def getPositionEpisode(dataBase, id_podcast):
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT position FROM episode WHERE id_podcast=? ORDER BY pub_date_ms DESC LIMIT 1", (id_podcast,))
    rows = cursor.fetchall()
    if len(rows) == 0:
        return 1
    else:
        return rows[0][0]+1


def lastPubDateEpisode(dataBase, id_podcast):
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT pub_date_ms FROM episode WHERE id_podcast=? ORDER BY pub_date_ms DESC LIMIT 1", (id_podcast,))
    rows = cursor.fetchall()
    if len(rows) == 0:
        return 0
    else:
        return rows[0][0]


def removeEpisodesReaded():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute(
        "SELECT Podcast.id,Podcast.title ,Episode.id,Episode.position from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE readed=1")
    for episode in cursor.fetchall():
        path = 'media/'+NAME_FORMAT.format(podcast_title=re.sub(
            '[^A-Za-z0-9]+', '', episode[1]), podcast_id=str(episode[0]), episode_position=str(episode[3]))
        if os.path.exists(path):
            print("Remove episode "+path)
            os.remove(path)
        path = 'media/'+NAME_FORMAT.format(podcast_title=re.sub(
            '[^A-Za-z0-9]+', '', episode[1]), podcast_id=str(episode[0]), episode_position=str(episode[3]))
        path = 'media/'+re.sub('[^A-Za-z0-9]+', '',
                               episode[1])+'_episode_'+str(episode[3])+'.mp3'
        if os.path.exists(path):
            print("Remove episode "+path)
            os.remove(path)
        cursor.execute(
            "UPDATE Episode SET downloaded = 0, sync_watch =0 WHERE id = ?", (episode[2],))
    dataBase.commit()
    dataBase.close()
    return {}


def downloadedEpisodesNotReaded():
    print('Downloaded episodes not readed')
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Podcast.id,Podcast.title,Podcast.image,Episode.id,Episode.title,Episode.position,Episode.url from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE readed=0 AND downloaded=0")
    for episode in cursor.fetchall():
        if(downloadEpisode(episode[0], episode[1], episode[2],
                           episode[4], episode[5], episode[6]) == True):
            cursor.execute(
                "UPDATE Episode SET downloaded = 1 WHERE id = ?", (episode[3],))
            dataBase.commit()
    print('End downloaded episodes not readed')
    dataBase.close()


def downloadEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("SELECT Podcast.id,Podcast.title,Podcast.image,Episode.id,Episode.title,Episode.position,Episode.url from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id WHERE Episode.id=? AND downloaded=0", (id,))
        rows = cursor.fetchall()
        if len(rows) == 1:
            episode = rows[0]
            if(downloadEpisode(episode[0], episode[1], episode[2],
                               episode[4], episode[5], episode[6]) == True):
                cursor.execute(
                    "UPDATE Episode SET downloaded = 1 WHERE id = ?", (episode[3],))
                dataBase.commit()
    dataBase.close()
    print('End')
    return {}


def downloadEpisode(podcastId, podcastTitle, podcastImage, episodeTitle, episodePosition, episodeUrl):
    path = 'media/'+NAME_FORMAT.format(podcast_title=re.sub(
        '[^A-Za-z0-9]+', '', podcastTitle), podcast_id=str(podcastId), episode_position=str(episodePosition))
    if not os.path.exists(path):
        with open('temp', 'wb') as f:
            print('Downloading=> '+podcastTitle + " épisode: "+episodeTitle)
            response = requests.get(episodeUrl, headers=HEADERS, stream=True)
            total_length = response.headers.get('content-length')

            if total_length is None:  # no content length header
                f.write(response.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]" %
                                     ('=' * done, ' ' * (50-done)))
                    sys.stdout.flush()
        if not os.path.exists("media"):
            os.mkdir("media")
        FNULL = open(os.devnull, 'w')
        if podcastImage not in (None, ''):
            with open("cover.png", 'wb') as f:
                print('\nDownloading cover=> ' +
                      podcastTitle + " épisode: "+episodeTitle)
                response = requests.get(
                    podcastImage, headers=HEADERS, stream=True)
                total_length = response.headers.get('content-length')
                type_image = response.headers.get('content-type')
                if('image' in type_image and response.status_code == requests.codes['ok']):
                    if total_length is None:  # no content length header
                        f.write(response.content)
                    else:
                        dl = 0
                        total_length = int(total_length)
                        for data in response.iter_content(chunk_size=4096):
                            dl += len(data)
                            f.write(data)
                            done = int(50 * dl / total_length)
                            sys.stdout.write("\r[%s%s]" %
                                             ('=' * done, ' ' * (50-done)))
                            sys.stdout.flush()
                else:
                    print('Downloading cover error=> '+str(response.status_code) +
                          " "+podcastTitle + " épisode: "+episodeTitle)
                    os.remove("cover.png")
        print('\nConverting')
        if(os.path.exists("cover.png")):
            im = Image.open('cover.png')
            im = im.resize((128, 128))
            im.save('cover.png')
            return_ffmpeg = subprocess.call('ffmpeg -i temp -i cover.png -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" -metadata title="'+EPISODE_NAME.format(episode_position=str(episodePosition), episode_title=re.sub('["]', '', episodeTitle))+'" -metadata artist="' +
                                            podcastTitle+'" -metadata album="'+podcastTitle+'"  -b:a 128k -acodec mp3 -y temp_conversion.mp3', shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
            os.remove("cover.png")
        else:
            return_ffmpeg = subprocess.call('ffmpeg -i temp -metadata title="'+EPISODE_NAME.format(episode_position=str(episodePosition), episode_title=re.sub(
                '["]', '', episodeTitle))+'" -metadata artist="'+podcastTitle+'" -metadata album="'+podcastTitle+'"  -b:a 128k -acodec mp3 -y temp_conversion.mp3', shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
        if(return_ffmpeg == 0):
            print('Converting success => ' +
                  podcastTitle + " épisode: "+episodeTitle)
            shutil.move("temp_conversion.mp3", 'media/'+NAME_FORMAT.format(podcast_title=re.sub(
                '[^A-Za-z0-9]+', '', podcastTitle), podcast_id=str(podcastId), episode_position=str(episodePosition)))
            os.remove("temp")
            return True
        else:
            print('Converting error => '+podcastTitle +
                  " épisode: "+episodeTitle)
            if(os.path.exists("temp_conversion.mp3")):
                os.remove("temp_conversion.mp3'")
            os.remove("temp")
            return False
    else:
        return True
