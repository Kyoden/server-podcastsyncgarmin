#!/usr/bin/env sh

set -euo pipefail

if [[ $# -ge 1 ]]; then
    sed -i -E "s/API_KEY = (.*)/API_KEY = \"$1\"/g" /opt/server-podcastsyncgarmin/api_listennotes.py &> /opt/server-podcastsyncgarmin/sed.log
fi

shift

cd /opt/server-podcastsyncgarmin/
source /opt/server-podcastsyncgarmin/venv/bin/activate
python3 server.py $@
