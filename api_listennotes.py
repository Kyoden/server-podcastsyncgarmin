#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8
# -*- coding: utf-8 -*-

import requests

API_KEY = ""

headers = {
    'X-ListenAPI-Key': API_KEY,
}


def searchPodcats(name):
    url = 'https://listen-api.listennotes.com/api/v2/search?q=' + \
        name+'&type=podcast&only_in=title'
    response = requests.request('GET', url, headers=headers)
    return response.json()


def getPodcast(id_podcast, next_episode_pub_date):
    url = 'https://listen-api.listennotes.com/api/v2/podcasts/'+id_podcast + \
        '?next_episode_pub_date=' + \
        str(next_episode_pub_date)+'&sort=oldest_first'
    response = requests.request('GET', url, headers=headers)
    return response.json()
