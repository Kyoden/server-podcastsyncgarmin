# Why server ?
The garmin watch as well as the Android application connects to the server to have a common database, the server is mandatory for synchronization for the Garmin watch and not having the money to pay for a server for X people with X data for now you have to start the server yourself.

# Enter a API key
Request an API key on https://www.listennotes.com/api/

Log in, you can just request a free account, you are entitled to 5000 requests per month.
After you will find your token in your dashboard or at the following address: https://www.listennotes.com/api/dashboard/#apps

In the file **api_listennotes.py** replace YOUR_KEY with the token that you have just recovered.

# Without API key 
Without API key you cannot search to add podcasts but you can still manage them by directly adding the rss feed of the podcast you want to follow. You can do it directly on the Android app of course

# Installation

## Python 3
https://www.python.org/downloads/

## Requirements
Execute the following command 
```bash
pip3 install -r requirements.txt
```
Or if you have defined the python3 command as python
```bash
pip install -r requirements.txt
```

## ffmpeg

https://www.ffmpeg.org/download.html

or

Mac (using [homebrew](http://brew.sh)):

```bash
brew install ffmpeg
```

Linux (using aptitude):

```bash
apt-get install ffmpeg
```

Windows:

1. Download and extract from [Windows binaries provided here](https://www.ffmpeg.org/download.html#build-windows).

# Launch server

Execute the following command 
```bash
python3 server.py
```
Or if you have defined the python3 command as python
```bash
python server.py
```

On the Android application and on the Garmin Connect application, modify the url to indicate your server url.

On the Android application it is via the settings button at the top right then change the server url

On the Garmin Connect application it is via Garmin Connect or Garmin Express. There is a url parameter for the application.

If you launch on your computer, it's your IP

# Customization
## Rename episode name for watch Garmin
For more visibility the title of an episode for the watch is replaced by **épisode: {position}**
You can change this in **manager_podcast.py** by modifying the constant **EPISODE_NAME**
I chose to redefine this because on the watch the titles too long are not well managed, the server free if you are not of the same opinion you can modify it without problem.

## Options on the server

```bash
-p or --check_periodic
```
Allows you to activate or not the verification of new episodes periodically, by default every day the constant can be modified in the file **server.py**

```bash
-e or --check_new_episodes
```
Allows you to activate or not the verification of new episodes when starting the server

```bash
-f or --check_file
```
Allows you to activate or not the verification of exist episodes file when starting the server

```bash
-d or --auto_download
```
Allows you to download to the server all unread episodes at launch or when searching for new episodes

```bash
-r or --auto_remove
```
Allows you to delete on the server all episodes read at launch or when searching for new episodes

Example:
```bash
python3 server.py --check_periodic true --check_start true --auto_remove false --auto_download true
```
Or if you have defined the python3 command as python
```bash
python server.py --check_periodic true --check_start true --auto_remove false --auto_download true
```

# **Application** 

## Web
It is under construction and it may take some time because it is not my domain but you can manage your podcasts via the route YOUR_URL (by default http://0.0.0.0:5000)

## Android
https://play.google.com/store/apps/details?id=com.ravenfeld.garmin.podcasts

## iOS
If someone is available to help me, I am interested.

## Garmin Connect IQ
https://apps.garmin.com/fr-FR/apps/5c22c9d7-4f38-4e03-8897-ad393b705dad

# Question ?
Do not hesitate to send me an email to alexis.lecanu.garmin@gmail.com if you have any concerns
