#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8
# -*- coding: utf-8 -*-
from flask import request
from flask import Flask
from flask import render_template
from flask import send_from_directory
from flask import Response
from flask import redirect
from flask_caching import Cache
import os
import threading
import json
import logging
import sys
import getopt
import manager_podcast
import api_listennotes

# be careful the more you check the more the API quotas listennotes if you use will be impacted
CHECK_TIME = 86400  # 24 hours
CHECK_PERIODIC = False
CHECK_NEW_EPISODE = True
CHECK_FILE_EXIST = True
AUTO_DOWLOAD_NEW_EPISODE = False
AUTO_REMOVE_EPISODES_READED = True


def checkNewEpisodesPeriodic():
    lock.acquire()
    manager_podcast.checkNewEpisodes(CHECK_TIME)
    if(AUTO_REMOVE_EPISODES_READED):
        manager_podcast.removeEpisodesReaded()
    if(AUTO_DOWLOAD_NEW_EPISODE):
        manager_podcast.downloadedEpisodesNotReaded()
    lock.release()
    threading.Timer(CHECK_TIME, checkNewEpisodesPeriodic).start()


def startServer():
    if(CHECK_PERIODIC):
        checkNewEpisodesPeriodic()
    elif(CHECK_NEW_EPISODE):
        lock.acquire()
        manager_podcast.checkNewEpisodes(CHECK_TIME)
        lock.release()
    if(CHECK_FILE_EXIST):
        lock.acquire()
        manager_podcast.checkFileEpisodes()
        lock.release()
    if(AUTO_REMOVE_EPISODES_READED):
        lock.acquire()
        manager_podcast.removeEpisodesReaded()
        lock.release()
    if(AUTO_DOWLOAD_NEW_EPISODE):
        lock.acquire()
        manager_podcast.downloadedEpisodesNotReaded()
        lock.release()


config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "simple",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}
try:
    opts, args = getopt.getopt(sys.argv[1:], "p:e:f:d:r", [
                               "check_periodic=", "check_new_episodes=", "check_file=", "auto_download=", "auto_remove="])
except getopt.GetoptError:
    sys.exit(2)
for opt, arg in opts:
    if opt in ("-p", "--check_periodic"):
        if(arg == "true" or arg == "True"):
            CHECK_PERIODIC = True
        else:
            CHECK_PERIODIC = False
    elif opt in ("-e", "--check_new_episodes"):
        if(arg == "true" or arg == "True"):
            CHECK_START_SERVER = True
        else:
            CHECK_START_SERVER = False
    elif opt in ("-f", "--check_file"):
        if(arg == "true" or arg == "True"):
            CHECK_FILE_EXIST = True
        else:
            CHECK_FILE_EXIST = False
    elif opt in ("-d", "--auto_download"):
        if(arg == "true" or arg == "True"):
            AUTO_DOWLOAD_NEW_EPISODE = True
        else:
            AUTO_DOWLOAD_NEW_EPISODE = False
    elif opt in ("-r", "--auto_remove"):
        if(arg == "true" or arg == "True"):
            AUTO_REMOVE_EPISODES_READED = True
        else:
            AUTO_REMOVE_EPISODES_READED = False
lock = threading.Lock()
manager_podcast.createDatabase()
app = Flask(__name__)
app.config.from_mapping(config)
cache = Cache(app)
threading.Timer(1, startServer).start()


@app.route('/watch/podcast', methods=['GET'])
@cache.cached(timeout=1)
def get_list_podcast_watch():
    displayReaded = request.values.get("displayReaded")
    return Response(json.dumps(manager_podcast.listPodcastWatch(displayReaded)), status=200, mimetype='application/json')


@app.route('/watch/podcast/<int:id_podcast>/episodes', methods=['GET'])
@cache.cached(timeout=1)
def get_list_episode_watch(id_podcast):
    order = request.values.get("order")
    displayReaded = request.values.get("displayReaded")
    return Response(json.dumps(manager_podcast.listEpisodeWatch(id_podcast, order, displayReaded)), status=200, mimetype='application/json')


@app.route('/watch/sync', methods=['GET'])
@cache.cached(timeout=1)
def get_list_episode_sync_watch():
    return Response(json.dumps(manager_podcast.listEpisodeSyncWatch()), status=200, mimetype='application/json')


@app.route('/watch/start_sync', methods=['POST'])
def post_start_sync_watch():
    return Response(json.dumps(manager_podcast.resetSyncWatch()), status=200, mimetype='application/json')


@app.route('/podcast', methods=['GET'])
@cache.cached(timeout=1)
def get_list_podcast():
    return Response(json.dumps(manager_podcast.listPodcast(CHECK_TIME)), status=200, mimetype='application/json')


@app.route('/episode/readed', methods=['POST'])
def post_readed_episode():
    ids = request.values.get("ids")
    idsJon = request.json

    if(ids is None and idsJon is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    else:
        if(ids is None):
            ids = idsJon.get("ids")
            print("ids : "+ids)
            manager_podcast.notSyncWatchEpisodes(ids)
        return Response(json.dumps(manager_podcast.readedEpisodes(ids)), status=200, mimetype='application/json')


@app.route('/episode/not_readed', methods=['POST'])
def post_not_readed_episode():
    ids = request.values.get("ids")
    if(ids is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    else:
        return Response(json.dumps(manager_podcast.notReadedEpisodes(ids)), status=200, mimetype='application/json')


@app.route('/episode/sync_watch', methods=['POST'])
def post_sync_watch_episode():
    ids = request.values.get("ids")
    if(ids is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    else:
        return Response(json.dumps(manager_podcast.syncWatchEpisodes(ids)), status=200, mimetype='application/json')


@app.route('/episode/not_sync_watch', methods=['POST'])
def post_not_sync_watch_episode():
    ids = request.values.get("ids")
    if(ids is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    else:
        return Response(json.dumps(manager_podcast.notSyncWatchEpisodes(ids)), status=200, mimetype='application/json')


@app.route('/episode/download', methods=['POST'])
def post_download_episode():
    ids = request.values.get("ids")
    if(ids is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    else:
        threading.Thread(target=download_episode, args=(ids,)).start()
        return Response(json.dumps({}), status=200, mimetype='application/json')


def download_episode(ids):
    lock.acquire()
    manager_podcast.downloadEpisodes(ids)
    lock.release()


@app.route('/podcast/search', methods=['GET'])
@cache.cached(timeout=86400, query_string=True)
def get_search_podcast():
    name = request.values.get("name")
    if(name is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    elif api_listennotes.API_KEY in (None, ''):
        return Response(json.dumps({'error': 'missing API KEY'}), status=401, mimetype='application/json')
    else:
        return Response(json.dumps(api_listennotes.searchPodcats(name)), status=200, mimetype='application/json')


@app.route('/podcast/<id_podcast>/add', methods=['POST'])
def post_add_podcast(id_podcast):
    return Response(json.dumps(manager_podcast.addPodcastListennotes(id_podcast)), status=200, mimetype='application/json')


@app.route('/podcast/add', methods=['POST'])
def post_add_podcast_rss():
    id_podcast_rss = request.values.get("rss")
    id_podcast_listennotes = request.values.get("listennotes")
    
    if(id_podcast_rss is None and id_podcast_listennotes is None):
        return Response(json.dumps({'error': 'missing parameter'}), status=400, mimetype='application/json')
    elif(id_podcast_rss is None and id_podcast_listennotes is not None):
        return Response(json.dumps(manager_podcast.addPodcastListennotes(id_podcast_listennotes)), status=200, mimetype='application/json')
    elif(id_podcast_rss is not None and id_podcast_listennotes is None):
        if(manager_podcast.addPodcastRss(id_podcast_rss)==200):
            return Response(json.dumps({}), status=200, mimetype='application/json')
        else:
            return Response(json.dumps({'error': 'unable to add url'}), status=404, mimetype='application/json')
    else:
        return Response(json.dumps({'error': 'many parameter'}), status=400, mimetype='application/json')


@app.route('/podcast/<int:id_podcast>/remove', methods=['POST'])
def post_remove_podcast(id_podcast):
    return Response(json.dumps(manager_podcast.removePodcast(id_podcast)), status=200, mimetype='application/json')


@app.route('/episode/remove', methods=['POST'])
def post_remove_episodes():
    return Response(json.dumps(manager_podcast.removeEpisodesReaded()), status=200, mimetype='application/json')


@app.route('/episode/<int:id_episode>', methods=['GET'])
def downloadFile(id_episode):
    filename = manager_podcast.getFilename(id_episode)
    if(filename is None):
        print("id unknown")
        return Response(json.dumps({'error': 'id unknown'}), status=404, mimetype='application/json')
    else:
        if(os.path.exists("media/"+filename)):
            return send_from_directory(directory="media", filename=filename)
        else:
            print("media/"+filename+" missing")
            return Response(json.dumps({'error': 'file '+filename+" missing"}), status=404, mimetype='application/json')


@app.route('/', methods=['GET'])
def webUIIndex():
    return render_template("index.html")


@app.route('/ui/podcast/add', methods=['GET'])
def webUIPodcastAdd():
    return render_template("podcast-add.html")


@app.route('/ui/podcast/manage', methods=['GET'])
def webUIPodcastManage():
    podcasts = manager_podcast.listPodcast(no_episodes=True)
    return render_template("podcast-manage.html", podcasts=podcasts)

@app.route('/ui/podcast/check_new_episodes', methods=['GET'])
def webUICheckNewEpisodes():
    manager_podcast.checkNewEpisodes(0)
    return redirect('/')

@app.route('/ui/podcast/remove_episodes_readed', methods=['GET'])
def webUIRemoveEpisodesReaded():
    manager_podcast.removeEpisodesReaded()
    return redirect('/')

@app.route('/ui/podcast/downloaded_episodes_not_readed', methods=['GET'])
def webUIDownloadedEpisodesNotReaded():
    manager_podcast.downloadedEpisodesNotReaded()
    return redirect('/')

@app.route('/ui/podcast/manage/<int:id_podcast>', methods=['GET'])
def webUIPodcastManageById(id_podcast):
    podcast = manager_podcast.podcast(id_podcast)
    return render_template("podcast-manage-by-id.html", podcast=podcast)

if __name__ == '__main__':
    # logging.basicConfig(filename='error.log',level=logging.DEBUG)
    port = os.getenv('PORT', '5000')
    app.run(debug=True, use_reloader=False, host='0.0.0.0', port=port)
