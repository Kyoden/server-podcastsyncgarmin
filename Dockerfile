FROM python:3.8.3-alpine3.12 AS build

RUN mkdir -p /opt/server-podcastsyncgarmin
COPY requirements.txt /opt/server-podcastsyncgarmin/

RUN \
    apk add --no-cache \
        build-base \
        python3-dev \
        # Pillow dependencies
        jpeg-dev \
        zlib-dev \
        freetype-dev \
        lcms2-dev \
        openjpeg-dev \
        tiff-dev \
        tk-dev \
        tcl-dev \
        harfbuzz-dev \
        fribidi-dev \
    && pip3 install virtualenv \
    && virtualenv /opt/server-podcastsyncgarmin/venv \
    && source /opt/server-podcastsyncgarmin/venv/bin/activate \
    && pip3 install -r /opt/server-podcastsyncgarmin/requirements.txt


FROM python:3.8.3-alpine3.12 as final

RUN \
    mkdir -p \
        /opt/server-podcastsyncgarmin/db \
        /opt/server-podcastsyncgarmin/media \
        /opt/server-podcastsyncgarmin/templates \
    && adduser garmin --disabled-password \
    && chown -R garmin /opt/server-podcastsyncgarmin \
    && apk add --no-cache \
        ffmpeg \
        # Pillow dependencies
        jpeg \
        zlib \
        freetype \
        lcms2 \
        openjpeg \
        tiff \
        tk \
        tcl \
        harfbuzz \
        fribidi

COPY requirements.txt /opt/server-podcastsyncgarmin/
COPY docker-entrypoint.sh /opt/server-podcastsyncgarmin/
COPY --from=build /opt/server-podcastsyncgarmin/venv /opt/server-podcastsyncgarmin/venv
COPY templates/* /opt/server-podcastsyncgarmin/templates/
COPY *.py /opt/server-podcastsyncgarmin/

USER garmin

EXPOSE 5000
VOLUME /opt/server-podcastsyncgarmin/db
VOLUME /opt/server-podcastsyncgarmin/media

ENTRYPOINT [ "/opt/server-podcastsyncgarmin/docker-entrypoint.sh" ]
CMD [ "" ]
